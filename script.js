const images = document.querySelectorAll('.image-to-show');
let currentIndex = 0;
let intervalId;

function showImage(index) {
    images.forEach(image => image.style.display = 'none');
    images[index].style.display = 'block';
}

function startSlideshow() {
    showImage(currentIndex);
    intervalId = setInterval(() => {
        currentIndex = (currentIndex + 1) % images.length;
        showImage(currentIndex);
    }, 3000);
}

function stopSlideshow() {
    clearInterval(intervalId);
}

function resumeSlideshow() {
    startSlideshow();
}

const startButton = document.getElementById('startButton');
const stopButton = document.getElementById('stopButton');
const resumeButton = document.getElementById('resumeButton');

startButton.addEventListener('click', () => {
    startSlideshow();
});

stopButton.addEventListener('click', () => {
    stopSlideshow();
});

resumeButton.addEventListener('click', () => {
    resumeSlideshow();
});